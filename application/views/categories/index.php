<h4><?= $title ?></h4>
<ul class="list-group">
<?php foreach($categoires as $category) : ?>
	<li class="list-group-item">
		<a href="<?php echo site_url('/categories/post/'.$category['id']); ?>"><?php echo $category['name']; ?></a>
		<?php if($this->session->userdata('user_id') ==  $category['user_id']): ?>
			<?php echo form_open('categories/delete/'.$category['id'], array('style'=>'display:inline')); ?>
				<button class="btn btn-sm btn-outline-danger">Delete</button>
			<?php echo form_close();?>
		<?php endif; ?>
	</li>
<?php endforeach; ?>
</ul>
