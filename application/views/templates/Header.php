<html>
<head>
	<title>
		Them ctf writeups
	</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.min.css">
	<script src="http://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#">Navbar</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarColor02">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="<?php echo base_url();?>">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>about">About</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>posts">Blog</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>categories">Categories</a>
			</li>
		</ul>
		<ul class="nav navbar-nav  navbar-right">
			<?php if(!$this->session->userdata('logged_in')) :?>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url();?>users/register">Register</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url();?>users/login">Login</a>
				</li>
			<?php endif; ?>
			<?php if($this->session->userdata('logged_in')) :?>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>posts/create">Create Post</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>categories/create">Create Category</a>
			</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url();?>users/profile">Profile</a>
				</li>
			<li class="nav-item">
				<a class="nav-link" href="<?php echo base_url();?>users/logout">logout</a>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</nav>
<br><br>
	<div class="container">
<!--	Flash data-->
		<?php if($this->session->flashdata('users_registered')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata("user_registered").'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_created')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_updated')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_updated').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('category_created')): ?>
			<?php echo '<p class="alert alert-success">'.$this->session->flashdata('category_created').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('post_delete')): ?>
			<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('post_delete').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('user_logedin')): ?>
		<?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_logedin').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('login_failed')): ?>
			<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('logged_out')): ?>
			<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('logged_out').'</p>';?>
		<?php endif; ?>

		<?php if($this->session->flashdata('category_deleted')): ?>
			<?php echo '<p class="alert alert-danger">'.$this->session->flashdata('category_deleted').'</p>';?>
		<?php endif; ?>
