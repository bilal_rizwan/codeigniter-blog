<h2><?= $title ?></h2>
<?php foreach($posts as $post): ?>
	<?php echo "<h4>".$post['title']."</h4>"; ?>
<div class="row">
	<div class="col-md-3">
		<img style="width:70%" src="<?php echo site_url();?>assets/images/posts/<?php echo $post['post_image'];?>">
	</div>
	<div class="col-md-9">
		<?php
		{

			echo "<small>Date Created : ". $post['created_at'] ."</small> <strong> in ".$post['name']."</strong><br>";
			echo "<p>".word_limiter($post['body'],70)."</p><br>";
			echo "<a href=".site_url('/posts/'.$post['slug']).">Read More</a><br></br>";
		}
		?>
	</div>
</div>
<?php endforeach; ?>
