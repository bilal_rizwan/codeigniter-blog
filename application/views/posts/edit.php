<h3><?= $title ?></h3>

<?php echo validation_errors(); ?>
<?php echo form_open('posts/update'); ?>
	<input type="hidden" name="id" value="<?php echo $post['id']; ?>">
<div class="form-group">
	<label>Title</label>
	<input type="text" class="form-control" name="title" placeholder="Enter Title" value="<?php echo $post['title'];?>">
	<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
</div>
<div class="form-group">
	<label>Body</label>
	<textarea id="editor1" type="text" class="form-control" name="body"><?php echo $post['body'];?></textarea>
</div>
<label>Categories</label>
<select name="category_id" class="form_control">
<?php foreach($categories as $category): ?>
	<option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
<?php endforeach; ?>
</select>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
