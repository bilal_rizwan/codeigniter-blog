<br>
<div class="jumbotron">
	<h2><?= $title ?></h2>
	<small><?= $post['created_at']?></small>
	<p><?= $post['body']?></p>
</div>
<?php if($this->session->userdata('user_id') == $post['user_id']) : ?>
	<a class="btn btn-default pull-left" href="<?php echo base_url();?>posts/edit/<?php echo $post['slug'];?>">Edit</a>
	<?php echo form_open('/posts/delete/'.$post['id']); ?>
		<input type="submit" value="delete" class="btn btn-danger">
	</form>
<hr>
<?php endif; ?>

<h4>Comments</h4>
<?php if($comments): ?>
	<?php foreach($comments as $comment) : ?>
<!--<div class="list-group">-->
<!--	<a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">-->
<!--		<div class="d-flex w-100 justify-content-between">-->
<!--			<small>3 days ago</small>-->
<!--		</div>-->
<!--		<p class="mb-1">--><?php //echo $comment['body'];?><!--</p>-->
<!--		<small>--><?php //echo $comment['name'];?><!--</small>-->
<!--	</a>-->
<!--</div>-->
		<div class="list-group">
			<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
				<div class="d-flex w-100 justify-content-between">
					<h5 class="mb-1"></h5>
					<small class="text-muted">3 days ago</small>
				</div>
				<p class="mb-1"><?php echo $comment['body'];?></p>
				<small class="text-muted"><?php echo $comment['name'];?></small>
			</a>
		</div>
	<?php endforeach; ?>
<?php else : ?>
	<p>No Comments to Display</p>
<?php endif; ?>
<hr>
<?php echo validation_errors(); ?>
<?php echo form_open('comments/create/'.$post['id']);?>
<div class="form-group">
	<label>Name</label>
	<input type="text" name="name" class="form-control">
</div>
<div class="form-group">
	<label>Email</label>
	<input type="text" name="email" class="form-control">
</div>
<div class="form-group">
	<label>Body</label>
	<textarea type="text" name="body" class="form-control"></textarea>
</div>
<input type="hidden" name="slug" value="<?php echo $post['slug'];?>">
<button type="submit" class="btn btn-primary">Submit</button>
</form>
