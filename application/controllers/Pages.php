<?php
class Pages extends  CI_Controller
{
	public function views($page = 'home')
	{
		$data['title'] =ucfirst($page);

		$this->load->view('templates/Header');
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/Footer');
	}
}
