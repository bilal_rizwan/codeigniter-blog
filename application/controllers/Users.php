<?php

class Users extends CI_Controller
{
	public function register()
	{
		$data['title'] = "Sign up";

		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','Email','required|callback_check_email_exists');
		$this->form_validation->set_rules('password','Password2','required');
		$this->form_validation->set_rules('username','Username','required|callback_check_username_exists');
		$this->form_validation->set_rules('password2','Confirm Password','required');

		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/Header');
			$this->load->view('users/register',$data);
			$this->load->view('templates/Footer');
		}
		else
		{
			//Encryption method md5
			$enc_password = md5($this->input->post('password'));

			$this->users_model->register($enc_password);
			$this->session->set_flashdata('users_registered','User Created');
			redirect('posts');
		}
	}

	function check_username_exists($username)
	{
		$this->form_validation->set_message('check_username_exists','This username is already taken');
		if($this->users_model->check_username_exists($username))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	function check_email_exists($email)
	{
		$this->form_validation->set_message('check_email_exists','This email is already taken');
		if($this->users_model->check_email_exists($email))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function login()
	{
		$data['title'] = "Sign in";

		$this->form_validation->set_rules('password','Password','required');
		$this->form_validation->set_rules('username','Username','required');



		if($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/Header');
			$this->load->view('users/login',$data);
			$this->load->view('templates/Footer');
		}
		else
		{
			//Get username & pass
			$username = $this->input->post('username');
			$pass = md5($this->input->post('password'));
			$user_id = $this->users_model->login($username,$pass);

			//if user exists
			if($user_id)
			{
				//create session else error
				$user_data= array(
					'user_id'=>$user_id,
					'username'=> $username,
					'logged_in' => true
				);

				$this->session->set_userdata($user_data);
				//set message
				$this->session->set_flashdata('user_logedin','You are now logged in');
				redirect('posts');
			}
			else
			{
				$this->session->set_flashdata('login_failed','Login is invalid');
				redirect('users/login');
			}



		}
	}

	// to logout
	public function logout()
	{
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('username');

		//logout message
		$this->session->set_flashdata('logged_out','User now logged out');

		//redirect
		redirect('users/login');

	}


	public function profile()
	{
		if (!$this->session->userdata('logged_in')) {
			redirect('users/login');
		}else
		{
			$data['uname'] = $this->session->userdata('username');
			$this->load->view('templates/Header');
			$this->load->view('users/profile',$data);
			$this->load->view('templates/Footer');
		}
	}
}
