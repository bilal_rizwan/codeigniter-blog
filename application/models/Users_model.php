<?php

class Users_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function register($enc_pass)
	{
		$data = array(
		 'name' => $this->input->post('name'),
		 'email' => $this->input->post('email'),
		 'username' => $this->input->post('username'),
		 'password' => $enc_pass,
		 'zipcode' => $this->input->post('zipcode')

		);

		return $this->db->insert('users', $data);
	}

	public function check_username_exists($username)
	{
		$query = $this->db->get_where('users',array('username'=>$username));
		if(empty($query->row_array()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function check_email_exists($email)
	{
		$query = $this->db->get_where('users',array('email'=>$email));
		if(empty($query->row_array()))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function login($username,$pass)
	{
		$this->db->where('username',$username);
		$this->db->where('password',$pass);

		$result = $this->db->get('users');

		if($result->num_rows() == 1)
		{
			return $result->row(0)->id;
		}else
		{
			return false;
		}
	}

	// get logged in user name

//	public function get_name()
//	{
//		$this->db->order_by('id');
//		return $this->db->get_where('users',array('user_id'=>$this->session->userdata('user_id')))->row()->name;
//	}




}
